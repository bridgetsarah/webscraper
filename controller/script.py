# Example Python Scripting Snippets

# Loops
for i in range(1, 20):
    print(i)
else:
    print('the loop is over') 

# User Input Function ---------------------
def userinput():
    input(" what'your name? ")
    input(" Whats your date of birth? ")                        # Validation required to verify over 18+
    input(" Whats your email address? ")                        # Validation requred to verify (active email address)
    input(" Select country?")                                   # Selection List Avaliable? Tuples or Dictionary?
    print(input)


# User Login Function ---------------------
def userLogin(true = False):                                    # Review on true statements and elif statements + Validation
    if userLogin = True;
        print(" You sucessfully logged in! ")
    elif userLogin = False;
        print(" Bad luck mate! Try again. ")
    else
        print(" Retrive lost password. ")    
